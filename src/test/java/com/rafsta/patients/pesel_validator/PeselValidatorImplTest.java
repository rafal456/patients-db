package com.rafsta.patients.pesel_validator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

class PeselValidatorImplTest {
    List<String> peselToTest;
    List<String> wrongPeselList;
    PeselValidatorImpl pv;

    @BeforeEach
    void init() {
        peselToTest = new ArrayList<>();
        wrongPeselList = new ArrayList<>();
        try {
            Files.lines(Paths.get("src/test/resources/pesel-examples.txt")).forEach(s -> peselToTest.add(s));
            Files.lines(Paths.get("src/test/resources/wrong-pesel-examples.txt")).forEach(s -> wrongPeselList.add(s));
        } catch (IOException e) {
            e.printStackTrace();
        }
        pv = new PeselValidatorImpl();
    }


    @Test
    void shouldPassWhenPeselIsCorrect() {

        for (String pesel : peselToTest) {
            boolean correctPesel = pv.isPesel(pesel);
            if (!correctPesel) System.err.println(pesel);
            assertThat(correctPesel).isTrue();
        }
    }

    @Test
    void shouldPassWhenPeselIsNotCorrect() {
        for (String pesel : wrongPeselList) {
            boolean correctPesel = pv.isPesel(pesel);
            if (correctPesel) System.err.println(pesel);
            assertThat(correctPesel).isFalse();
        }
    }

    @Test
    void wontPassWhenPeselSizeIsNotCorrect() {
        Class<PeselValidatorImpl> peselValidatorClass = PeselValidatorImpl.class;
        try {
            Method hasSizeOfPesel = peselValidatorClass.getDeclaredMethod("hasSizeOfPesel", String.class);
            hasSizeOfPesel.setAccessible(true);

            for (String pesel : peselToTest)
                assertTrue((Boolean) hasSizeOfPesel.invoke(pv, pesel));
        } catch (Exception e) {
            e.getMessage();
            fail();
        }
    }


    @Test
    void shouldPassIfParsingToIntSucceed() {
        try {
            Method parseNumbersFromPesel = PeselValidatorImpl.class.getDeclaredMethod("parseNumbersFromPesel", String.class);
            parseNumbersFromPesel.setAccessible(true);
            for (String pesel : peselToTest)
                assertThat(parseNumbersFromPesel.invoke(pv, pesel));
        } catch (Exception e) {
            e.getMessage();
            fail();
        }
    }

    @Test
    void shouldPassWithCorrectControlNumber() {
        Class<PeselValidatorImpl> peselValidatorClass = PeselValidatorImpl.class;
        try {
            Method validateControlNumber = peselValidatorClass.getDeclaredMethod("validateControlNumber", String.class);
            validateControlNumber.setAccessible(true);

            for (String pesel : peselToTest)
                assertThatCode(() -> validateControlNumber.invoke(pv, pesel)).doesNotThrowAnyException();
        } catch (Exception e) {
            e.getMessage();
            fail();
        }
    }

}