package com.rafsta.patients.pesel_validator;

public class WrongPeselException extends Exception {

    public WrongPeselException(String s) {
        super(s);
    }
}
