package com.rafsta.patients.pesel_validator;

public interface PeselValidator {

    boolean isPesel(String number);
}
