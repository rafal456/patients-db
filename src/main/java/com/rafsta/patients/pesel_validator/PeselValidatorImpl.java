package com.rafsta.patients.pesel_validator;

import org.jetbrains.annotations.NotNull;

import java.time.DateTimeException;
import java.time.LocalDateTime;


public class PeselValidatorImpl implements PeselValidator {
    public static final int PESEL_LENGTH = 11;
    private LocalDateTime currentDateFromPesel;

    public LocalDateTime getCurrentDateFromPesel() {
        return currentDateFromPesel;
    }

    public boolean isPesel(String nr) {
        if (notContainsChars(nr) && hasSizeOfPesel(nr)) {
            try {
                validateDateOfBirth(nr);
                validateControlNumber(nr);
                return true;
            } catch (WrongPeselException | DateTimeException e) {
                e.getMessage();
            }
        } return false;
    }

    private void validateDateOfBirth(String nr) throws DateTimeException {
        int[] peselInputs = parseNumbersFromPesel(nr);
        int year = 1900 + peselInputs[0] * 10 + peselInputs[1] + peselInputs[2] / 2 * 100;
        int month= calculateMonth(peselInputs);
        int day = peselInputs[4] * 10 + peselInputs[5];

        currentDateFromPesel = LocalDateTime.of(year,month,day,0,0);
    }

    private int calculateMonth(int[] peselInputs) {
        int month = peselInputs[2] * 10 + peselInputs[3];
        while (month>20) month-=20;
        return month;
    }


    private void validateControlNumber(String nr) throws WrongPeselException {
        int[] peselInputs = parseNumbersFromPesel(nr);
        int calculatedControlNumber = calculateControlNumber(peselInputs);
        if (peselInputs[10] != calculatedControlNumber) {
            throw new WrongPeselException("Pesel validation failed");
        }

    }

    private int calculateControlNumber(int[] peselInputs) {
        int[] keys = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};
        multiplyByKeyValues(peselInputs, keys);
        cutDozensDigit(peselInputs);
        int result = 0;
        for (int i = 0; i < PESEL_LENGTH - 1; i++) {
            result += peselInputs[i];
        }
        return 10 - (result % 10);
    }

    private void cutDozensDigit(int[] peselNumbers) {
        for (int i = 0; i < PESEL_LENGTH - 1; i++) {
            peselNumbers[i] %= 10;
        }
    }

    private void multiplyByKeyValues(int[] peselNumbers, int[] keys) {
        for (int i = 0; i < PESEL_LENGTH - 1; i++) {
            peselNumbers[i] *= keys[i];
        }
    }

    public int[] parseNumbersFromPesel(String nr) {
        int[] numbers = new int[PESEL_LENGTH];
        for (int i = 1; i <= PESEL_LENGTH; i++) {
            numbers[i - 1] = Integer.parseInt(nr.substring(i - 1, i));
        }
        return numbers;
    }

    private boolean notContainsChars(@NotNull String text) {
        for (int i = 0; i < text.length(); i++) {
            String indexedNumber = String.valueOf(text.charAt(i));
            try {
                Integer.parseInt(indexedNumber);
            } catch (NumberFormatException e) {
                return false;
            }
        }
        return true;
    }

    private boolean hasSizeOfPesel(String text) {
        String pesel = trimPesel(text);
        return pesel.length() == PESEL_LENGTH;
    }


    private String trimPesel(@NotNull String text) {
        return text.trim();
    }
}
